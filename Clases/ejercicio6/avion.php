<?php
include_once('transporte.php');

class Avion extends Transporte {
    private $numero_turbinas;

    public function __construct($nom, $vel, $com, $tur) {
        parent::__construct($nom, $vel, $com);
        $this->numero_turbinas = $tur;
    }

    public function resumenAvion() {
        $mensaje = parent::crear_ficha();
        $mensaje .= '<tr>
                        <td>Numero de turbinas:</td>
                        <td>' . $this->numero_turbinas . '</td>                
                    </tr>';
        return $mensaje;
    }
}
?>
