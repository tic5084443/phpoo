<?php

// Archivo: Clases/Carro2.php

class Carro2 {
    // Declaración de propiedades
    public $color;
    public $modelo;

    // Método de verificación
    public function verificacion($añoFabricacion) {
        // Lógica de verificación según la tabla proporcionada
        if ($añoFabricacion < 1990) {
            return 'No';
        } elseif ($añoFabricacion >= 1990 && $añoFabricacion <= 2010) {
            return 'Revisión';
        } else {
            return 'Sí';
        }
    }
}
?>




