<?php
include 'moto.php';
include 'carro.php';

$miMoto = new Moto;
$Carro1 = new Carro;

$mensajeParaFrontMoto = '';
$mensajeParaFrontCarro = '';

if (!empty($_POST)) {
    $miMoto->tipo = $_POST['tipo'];
    $miMoto->tamaño = $_POST['tamaño'];
    $mensajeParaFrontMoto = 'Se ha seleccionado una moto de tipo ' . $miMoto->tipo . ' y tamaño ' . $miMoto->tamaño;
}

if (!empty($_POST)) {
    $Carro1->color = $_POST['color'];
    $mensajeParaFrontCarro = 'El servidor dice que ya escogiste un color: ' . $_POST['color'];
}
?>

<form method="POST" action="">
    <label for="tipo">Tipo de moto:</label>
    <input type="text" name="tipo" id="tipo" required>

    <label for="tamaño">Tamaño de la moto:</label>
    <input type="text" name="tamaño" id="tamaño" required>

    <input type="submit" value="Enviar">
</form>

<form method="POST" action="">
    <label for="color">Color del carro:</label>
    <input type="text" name="color" id="color" required>

    <input type="submit" value="Enviar">
</form>

<?php
echo '<input type="text" value="' . $mensajeParaFrontMoto . '" readonly>';
echo '<input type="text" value="' . $mensajeParaFrontCarro . '" readonly>';
?>
