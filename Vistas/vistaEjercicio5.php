<?php
include_once('../clases/ejercicio5/token.php');

$mensaje = '';

if (!empty($_POST)) {
    $token1 = new Token($_POST['nombre']);
    $mensaje = $token1->mostrar();
}
?>
<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/bootstrap-grid.css">
    <script type="text/javascript" src="../js/jquery-3.4.1.min.js"></script>
    <script type="text/javascript" src="../js/bootstrap.min.js"></script>
    <title>Recoge tu token</title>
</head>
<body>
    <div class="container" style="margin-top: 4em">
        <header><h1>Recoge tu token</h1></header><br>
        <form method="post">
            <div class="form-group">
                <label for="CajaTexto1">Escribe tu nombre:</label>
                <input class="form-control" type="text" name="nombre" id="CajaTexto1">
            </div>
            <button class="btn btn-primary" type="submit">Enviar</button>
            <a class="btn btn-link offset-md-8 offset-lg-9 offset-6" href="../index.php">Regresar</a>
        </form>
        <input class='form-control' type='text' value='<?= $mensaje ?>' readonly>
    </div>
</body>
</html>
