<?php
include 'Clases/Carro2.php';

$Carro1 = new Carro2();
$mensajeVerificacion = '';

if (!empty($_POST)) {
    $Carro1->color = $_POST['color'];
    $Carro1->modelo = $_POST['modelo'];

    $añoFabricacion = date('Y', strtotime($_POST['fechaFabricacion']));
    $resultadoVerificacion = $Carro1->verificacion($añoFabricacion);

    $Carro1->resultadoVerificacion = $resultadoVerificacion;

    $mensajeVerificacion = "Color: {$Carro1->color}, Modelo: {$Carro1->modelo}, Verificación: {$resultadoVerificacion}";
}
?>

<form method="POST" action="">
    <label for="color">Color del carro:</label>
    <input type="text" name="color" id="color" required>

    <label for="modelo">Modelo del carro:</label>
    <input type="text" name="modelo" id="modelo" required>

    <label for="fechaFabricacion">Año de fabricación:</label>
    <input type="month" name="fechaFabricacion" id="fechaFabricacion" required>

    <input type="submit" value="Enviar">
</form>

<table border="1">
    <tr>
        <th>Información del Carro</th>
        <th>Resultado de Verificación</th>
    </tr>
    <tr>
        <td><?php echo $mensajeVerificacion; ?></td>
        <td><?php echo $Carro1->resultadoVerificacion; ?></td>
    </tr>
</table>
