<?php
include_once('../clases/ejercicio6/carro.php');
include_once('../clases/ejercicio6/avion.php');
include_once('../clases/ejercicio6/barco.php');
include_once('../clases/ejercicio6/nuevoTransporte.php');
?>

<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/bootstrap-grid.css">
    <script type="text/javascript" src="../js/jquery-3.4.1.min.js"></script>
    <script type="text/javascript" src="../js/bootstrap.min.js"></script>
    <title>Transportes</title>
</head>
<body>
    <div class="container" style="margin-top: 4em">
        <header><h1>Transportes</h1></header><br>
        <form method="post">
            <div class="form-group">
                <label for="tipo_transporte">Selecciona el tipo de transporte:</label>
                <select class="form-control" name="tipo_transporte" id="tipo_transporte" required>
                    <option value="aereo">Avión</option>
                    <option value="terrestre">Carro</option>
                    <option value="maritimo">Barco</option>
                </select>
            </div>
            <div class="form-group">
                <label for="nombre">Nombre:</label>
                <input class="form-control" type="text" name="nombre" id="nombre" required>
            </div>
            <div class="form-group">
                <label for="velocidad_max">Velocidad Máxima:</label>
                <input class="form-control" type="text" name="velocidad_max" id="velocidad_max" required>
            </div>
            <div class="form-group">
                <label for="tipo_combustible">Tipo de Combustible:</label>
                <input class="form-control" type="text" name="tipo_combustible" id="tipo_combustible" required>
            </div>
            <!-- Adición del nuevo transporte -->
            <div class="form-group">
                <label for="nuevo_transporte">Nuevo Transporte:</label>
                <input class="form-control" type="text" name="nuevo_transporte" id="nuevo_transporte" required>
            </div>
            <button class="btn btn-primary" type="submit">Enviar</button>
            <a class="btn btn-link offset-md-8 offset-lg
